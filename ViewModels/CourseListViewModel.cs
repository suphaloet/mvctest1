﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.ViewModels;
namespace Test.ViewModels
{
    public class CourseListViewModel
    {
        public List<CourseViewModel> course { get; set; }
    }
}