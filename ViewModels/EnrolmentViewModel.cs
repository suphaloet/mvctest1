﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.ViewModels
{
    public class EnrolmentViewModel
    {
        public int ID { get; set; }
        public string StudentID { get; set; }
        public string CourseNo { get; set; }
        public string Grade { get; set; }
    }
}