﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Models;
namespace Test.ViewModels
{
    public class EnrolmentListViewModel
    {
        public List<EnrolmentViewModel> enrolment { get; set; }
    }
}