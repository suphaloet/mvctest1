﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.ViewModels
{
    public class StudentViewModel
    {
        public string StudentID { get; set; }
        public string StudentName { get; set; }
        public string StudentYear { get; set; }
        public double StudentGPA { get; set; }
    }
}