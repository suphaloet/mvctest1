﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.ViewModels
{
    public class CourseViewModel
    {
        public string CourseNo { get; set; }
        public string CourseName { get; set; }
        public int Credit { get; set; }
    }
}