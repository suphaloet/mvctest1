﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test.ViewModels;
using Test.Models;

namespace Test.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            StudentListViewModel studentListViewModel = new StudentListViewModel();

            StudentLayer stdBal = new StudentLayer();
            List<Student> students = stdBal.GetStudent();

            List<StudentViewModel> stdViewModels = new List<StudentViewModel>();

            foreach (Student std in students)
            {
                StudentViewModel stdViewModel = new StudentViewModel();
                stdViewModel.StudentID = std.StudentID;
                stdViewModel.StudentName = std.FirstName + " " + std.Lastname;
                stdViewModel.StudentYear = std.Year;
                stdViewModel.StudentGPA = std.GPA;
                stdViewModels.Add(stdViewModel);
            }
            studentListViewModel.student = stdViewModels;
            return View("Index", studentListViewModel);
        }

        public ActionResult AddNew()
        {
            return View("CreateStudent");
        }
        public ActionResult SaveStudent(Student s, string BtnSubmit)
        {
            switch (BtnSubmit)
            {
                case "Save Student":
                    if(ModelState.IsValid)
                    {
                        StudentLayer stdBal = new StudentLayer();
                        stdBal.SaveStudent(s);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View("CreateStudent");
                    }
                    
                case "Cancel":
                    return RedirectToAction("Index");
            }
            return new EmptyResult();
        }
    }
}