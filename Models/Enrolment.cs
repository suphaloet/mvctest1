﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Test.ViewModels;
using Test.Models;
namespace Test.Models
{
    public class Enrolment
    {
        [Key]
        public int ID { get; set; }
        [StudentIDValidation]
        public string StudentID { get; set; }
        [CourseNoValidation]
        public string CourseNo { get; set; }
        [GradeValidation]
        public string Grade { get; set; }
    }
    public class GradeValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return new ValidationResult("Please provide Grade");
            }
            else
            {
                if (value.ToString() == "A")
                {
                    return ValidationResult.Success;
                }
                else if (value.ToString() == "B")
                {
                    return ValidationResult.Success;
                }
                else if (value.ToString() == "C")
                {
                    return ValidationResult.Success;
                }
                else if (value.ToString() == "D")
                {
                    return ValidationResult.Success;
                }
                else if (value.ToString() == "F")
                {
                    return ValidationResult.Success;
                }
                else
                {
                    return new ValidationResult("Grade must be A, B, C, D, F");
                }
            }
        }
    }

    public class StudentIDValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return new ValidationResult("Please provide Student ID");
            }
            else
            {
                StudentLayer stdLayer = new StudentLayer();
                bool checkID = stdLayer.CheckStudentID(value.ToString());
                if (checkID != true)
                {
                    return new ValidationResult("Student ID in enrolment must be existed in students");
                }
            }
            return ValidationResult.Success;
        }
    }
    public class CourseNoValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return new ValidationResult("Please provide Course No");
            }
            else
            {
                CourseLayer cLayer = new CourseLayer();
                bool checkNo = cLayer.CheckCourseNo(value.ToString());
                if (checkNo != true)
                {
                    return new ValidationResult("Course No in enrolment must be existed in Courses");
                }
            }
            return ValidationResult.Success;
        }
    }
}