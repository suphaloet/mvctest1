﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Test.Models
{
    public class Student
    {
        [Key]
        public string StudentID { get; set; }
        [FirstnameValidation]
        public string FirstName { get; set; }
        [LastnameValidation]
        public string Lastname { get; set; }
        public string Year { get; set; }
        public double GPA { get; set; }
    }

    public class FirstnameValidation:ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value==null)
            {
                return new ValidationResult("Please provide First Name");
            }
            else
            {
                if(value.ToString().Length != 10)
                {
                    return new ValidationResult("Frist Name enfore 10 charactors");
                }
                if(value.ToString().Contains("@"))
                {
                    return new ValidationResult("First Name should not contain @");
                }
            }
            return ValidationResult.Success;
        }
    }
    public class LastnameValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return new ValidationResult("Please provide Last Name");
            }
            else
            {
                if (value.ToString().Length != 10)
                {
                    return new ValidationResult("Last Name enfore 10 charactors");
                }
                if (value.ToString().Contains("@"))
                {
                    return new ValidationResult("Last Name should not contain @");
                }
            }
            return ValidationResult.Success;
        }
    }
}