﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Test.Models;
namespace Test.Models
{
    public class DataAccessLayer : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().ToTable("TblStudent");
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Courses>().ToTable("TblCourse");
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Enrolment>().ToTable("TblEnrolment");
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Student> student { get; set; }
        public DbSet<Courses> course { get; set; }
        public DbSet<Enrolment> enrolment { get; set; }
    }
}