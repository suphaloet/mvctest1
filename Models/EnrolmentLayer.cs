﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Models;
using Test.ViewModels;

namespace Test.Models
{
    public class EnrolmentLayer
    {
        public List<Enrolment> GetEnrolment()
        {
            DataAccessLayer DALErm = new DataAccessLayer();
            return DALErm.enrolment.ToList();
        }
        public Enrolment SaveEnrolment(Enrolment enl)
        {
            DataAccessLayer DALErm = new DataAccessLayer();
            DALErm.enrolment.Add(enl);
            DALErm.SaveChanges();
            changeGPA(enl.StudentID.ToString());
            return enl;
        }
        public void changeGPA(string StudentID)
        {
            List<Enrolment> Enrolments = this.GetEnrolment();
            CourseLayer courseLayer = new CourseLayer();
            StudentLayer stdLayer = new StudentLayer();
            double gpa;
            double Calgrade = 0;
            double credit = 0;
            double sumCredit = 0;
            double sumCalGrade = 0;
            foreach(Enrolment item in Enrolments)
            {
                if(StudentID == item.StudentID)
                {
                    credit = courseLayer.getCredit(item.CourseNo.ToString());
                    sumCredit += credit;
                    Calgrade = convertGrade(item.Grade.ToString()) * credit;
                    sumCalGrade += Calgrade;
                }
            }
            gpa = sumCalGrade / sumCredit;
            stdLayer.UpdateGPA(gpa, StudentID);
        }
        public double convertGrade(string grade)
        {
            if (grade == "A")
            {
                return 4;
            }
            else if (grade == "B")
            {
                return 3;
            }
            else if (grade == "C")
            {
                return 2;
            }
            else if (grade == "D")
            {
                return 1;
            }
            return 0;
        }
    }
}